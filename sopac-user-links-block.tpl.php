<? /* page added by CraftySpace */ ?>

<ul>
  <li class="leaf first"><a href="/user">Summary</a></li>
  <li class="leaf"><a href="/user/checkouts">My Checkouts</a></li>
  <li class="leaf"><a href="/user/holds">My Holds</a></li>
  <li class="leaf"><a href="/user/fines">My Fines</a></li>
  <li class="expanded last"><a href="/user/library">My Library</a>
    <ul class="menu">
      <li class="leaf first"><a title="" href="/user/library/ratings">Ratings</a></li>
      <li class="leaf"><a title="" href="/user/library/reviews">Reviews</a></li>
      <li class="leaf"><a title="" href="/user/library/tags">Tags</a></li>
      <li class="leaf last"><a title="" href="/user/library/searches">Searches</a></li>
    </ul>
  </li>
  <li class="leaf"><a title="" href="/catalog/search">Search Catalog</a></li>
  <li class="leaf"><a title="" href="/user/<?php print $uid ?>/edit">Edit Account</a></li>
  <li class="leaf last"><a href="/logout">Log out</a></li>
</ul>